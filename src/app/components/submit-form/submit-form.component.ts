import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ListFormComponent } from '../list-form/list-form.component';
import { Input } from '@angular/core';
@Component({
  selector: 'app-submit-form',
  templateUrl: './submit-form.component.html',
  styleUrls: ['./submit-form.component.css']
})
export class SubmitFormComponent implements OnInit {

  data: FormGroup
  disabledButton = true;
  isConfirmed = false;
  defaultAmount = 1000;
  isOverAmount = false;
  isOverRate = false;
  @Input()
  second: ListFormComponent;
  constructor(private fb: FormBuilder) {
  }
  ngOnInit() {
    this.data = this.fb.group({
      fromAccount: new FormControl('Anhpd5'),
      currency: ['EUR'],
      amount: new FormControl(this.defaultAmount),
      toAccount: new FormControl(null, Validators.required),
      rateInput: new FormControl('')
    });
  }
  onSubmit() {
    this.isConfirmed = true;

  }
  onTransfer(value) {
    this.second.setValue(value);
    this.isConfirmed = false;
    this.defaultAmount = this.defaultAmount - value.rateInput
    this.data = this.fb.group({
      fromAccount: new FormControl('Anhpd5'),
      currency: ['EUR'],
      amount: new FormControl(this.defaultAmount),
      toAccount: new FormControl(null, Validators.required),
      rateInput: new FormControl('')
    });
  }
  changeAmount(value) {
    if (value > 500) {
      this.isOverAmount = true;
      return;
    }
    if ((this.data.value.amount - value) < -500) {
      this.isOverRate = true;
      return
    }
    this.isOverRate = false;
    this.isOverAmount = false;
  }
}
