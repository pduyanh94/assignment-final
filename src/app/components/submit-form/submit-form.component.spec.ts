import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SubmitFormComponent } from './submit-form.component';
import { ListFormComponent } from '../list-form/list-form.component';
import { DatePipe } from '@angular/common';

describe('SubmitFormComponent', () => {
  let component: SubmitFormComponent;
  let fixture: ComponentFixture<SubmitFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SubmitFormComponent],
      imports: [FormsModule, ReactiveFormsModule],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const PipeStub = {
      transform: () => {

      },
      locale: () => {

      }
    };
    const driftInfo: ListFormComponent = new ListFormComponent(new DatePipe('en-US'));
    component.second = driftInfo;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should onSubmit', () => {
    spyOn(component, "onSubmit").and.callThrough();;
    component.onSubmit();
    expect(component.isConfirmed).toEqual(true);
  });
  it('should changeAmount', () => {
    spyOn(component, "changeAmount").and.callThrough();
    component.changeAmount(600);
    expect(component.isOverAmount).toEqual(true);
  });
  it('should changeAmount validate fail case over balance', () => {
    spyOn(component, "changeAmount").and.callThrough();;
    component.data.value.amount = -10
    component.changeAmount(500);
    expect(component.isOverRate).toEqual(true);
  });
  it('should onTransfer', () => {
    spyOn(component, "onTransfer").and.callThrough();
    let mockValue = {
      rateInput: 100,
      toAccount: 'abc'
    }
    component.onTransfer(mockValue);
    expect(component.isConfirmed).toEqual(false);
  });
});
