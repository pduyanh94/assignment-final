import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ListFormComponent } from './list-form.component';

describe('ListFormComponent', () => {
  let component: ListFormComponent;
  let fixture: ComponentFixture<ListFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListFormComponent],
      imports: [FormsModule, ReactiveFormsModule],
      providers: [DatePipe],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should filter', () => {
    spyOn(component, "filter").and.callThrough();
    component.filter('abc');
    expect(component.filter).toHaveBeenCalled();
  });
  it('should sortOrders with date', () => {
    spyOn(component, "sortOrders").and.callThrough();
    component.sortOrders('date');
    expect(component.sortOrders).toHaveBeenCalled();
    expect(component.sortReverse).toEqual(false)
  });
  it('should sortOrders with beneficiary', () => {
    spyOn(component, "sortOrders").and.callThrough();
    component.sortOrders('beneficiary');
    expect(component.sortOrders).toHaveBeenCalled();
    expect(component.sortReverse).toEqual(false)
  });
  it('should sortOrders with amount', () => {
    spyOn(component, "sortOrders").and.callThrough();
    component.sortOrders('amount');
    expect(component.sortOrders).toHaveBeenCalled();
    expect(component.sortReverse).toEqual(false)
  });
  it('should setValue', () => {
    spyOn(component, "setValue").and.callThrough();
    let mockData = {
      toAccount: 'abc',
      rateInput: 1000
    }
    component.setValue(mockData);
    expect(component.setValue).toHaveBeenCalled();
    console.log(component.folderObjs.length)

    expect(component.folderObjs.length).toEqual(12)
  });
});
