import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import mockData from '../../mock/transactions.json';
@Component({
  selector: 'app-list-form',
  templateUrl: './list-form.component.html',
  styleUrls: ['./list-form.component.css'],
})
export class ListFormComponent implements OnInit {
  search = '';
  list = mockData.data;

  folderObjs = [];
  sortType: string;
  sortReverse: boolean = false;
  constructor(private datePipe: DatePipe) { }
  ngOnInit() {
    this.filter();
    this.folderObjs.forEach(i => {
      i.dates.valueDate = this.datePipe.transform(i.dates.valueDate, 'MMM. dd');
    });
    this.sortOrders('date');
  }
  showImage() {
    return 'assets/icons/7-eleven.png';
  }
  filter(search: string = "") {
    search = search.trim(); // remove empty space 
    if (search) { // if the user send an empty string like spaces
      this.folderObjs = this.list.filter((v) => (v.merchant.name.toLowerCase() || '').includes(search.toLocaleLowerCase()));
    } else { // reset 
      this.folderObjs = this.list;
    }
  }
  sortOrders(property) {
    this.sortType = property;
    this.sortReverse = !this.sortReverse;
    this.folderObjs.sort(this.dynamicSort(property));
  }
  dynamicSort(property) {
    let sortOrder = -1;

    if (this.sortReverse) sortOrder = 1;

    return function (a, b) {
      switch (property) {
        case 'date':
          a[property] = a.dates.valueDate;
          b[property] = b.dates.valueDate;
          break;
        case 'beneficiary':
          a[property] = a.merchant.name;
          b[property] = b.merchant.name;
          break;
        case 'amount':
          a[property] = Number(a.transaction.amountCurrency.amount);
          b[property] = Number(b.transaction.amountCurrency.amount);
          break;
      }
      let result =
        a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0;

      return result * sortOrder;
    };
  }
  setValue(newValue: any) {
    let mockData = {
      "categoryCode": "#12a580",
      "dates": {
        "valueDate": 1600493600000
      },
      "transaction": {
        "amountCurrency": {
          "amount": 5000,
          "currencyCode": "EUR"
        },
        "type": "Manual transfer",
        "creditDebitIndicator": "CRDT"
      },
      "merchant": {
        "name": "Backbase",
        "accountNumber": "SI64397745065188826"
      }
    }
    mockData.merchant.name = newValue.toAccount;
    mockData.dates.valueDate = Date.now();
    mockData.transaction.amountCurrency.amount = newValue.rateInput;
    this.folderObjs.unshift(mockData);
    this.folderObjs.forEach(i => {
      i.dates.valueDate = this.datePipe.transform(i.dates.valueDate, 'MMMM. dd');
    });
  }
}
