# TransferMoney

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running coverage

Run `ng test --code-coverage` to show the code coverage

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Program structure
This program only has one screen so no routing is included.
I transfer all assets to the `assets` folder, move `transaction.json` into it aswell because it's hardcoded.
The components is simple from `App` --> `components` (cotains 3 components)
I create 3 components and each components has its functional(`header`,`list-form`,`submit-form`).
## Functionality
1. You can view the `Recent Transaction` immediately after you start but I include the year of the Transaction to make it less confusing. I sorted it as `Date` DESC at first.
2. Money transfer function is ready to use. The form validation is ready and provide error messages if the form is not valid. CLick `SUBMIT` lead you to preview screen (simple change from input field to readonly paragraph because there are no actual design for it). From the beginning `SUBMIT` button is disabled because required field was not filled
3. Also the value is only transfer to transaction list when you click `TRANSFER` button. The default color for new transactions is GREEN, totalBalance of user is reduced for each transfer.
4. The Filter bar is ready to use as well.
5. The Filter input field is responsive to every keyword that search `Merchant Name`
6. 3 Sort button is working well for filer `Date`, `Benefeciary` and `Amount`
7. The `transaction.json` does not contain the base64 images field as you said in the `readme.md` so I put 7-eleven as default.
8. Test Coverage: ~95% with `ng test --code-coverage`